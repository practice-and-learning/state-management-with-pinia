export interface Task {
    id: number | string,
    title: string,
    description: string,
    is_fav: boolean,
}