import {defineStore} from "pinia";

export const useUserStore = defineStore('userStore', {
    state: () => ({
        user: null as User | null
    }),
    getters: {
        isLoggedIn(state) {
            return state.user != null;
        }
    },
    actions: {
        setUser(user: User | null) {
            this.user = user;
        },
        logout() {
            this.user = null;
        }
    }
})