import {defineStore} from "pinia";
import {Task} from "@/models/Task";

export const userTaskStore = defineStore('taskStore', {
    state: () => ({
        tasks: [
            {id: 1, title: 'First Task', description: 'Its my favorites task to do work', is_fav: false},
            {id: 2, title: 'Second Task', description: 'Its my favorites task to do work', is_fav: true},
            {id: 3, title: 'Third Task', description: 'Its my favorites task to do work', is_fav: false},
        ] as Array<Task>
    }),
    getters: {
        getFavTasks(): Array<Task> {
            return this.tasks.filter((item) => item.is_fav);
        },
    },
    actions: {
        extracted: function (task: Task) {
            return this.tasks.findIndex((item: Task) => item.id == task.id);
        },
        addInFav(task: Task) {
            const index = this.extracted(task);
            this.tasks[index] = {...task, is_fav: true};
        },
        removeFromFavorite(task: Task) {
            const index = this.extracted(task);
            this.tasks[index] = {...task, is_fav: false};
        },
        deleteTask: function (task: Task) {
            const index = this.extracted(task);
            this.tasks.splice(index, 1)
        }
    }
})
