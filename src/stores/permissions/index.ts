import {defineStore} from "pinia";

export const usePermissionStore = defineStore('permissionStore', {
    state: () => ({
        permissions: ['allow-home']
    }),
})