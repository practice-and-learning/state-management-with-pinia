import {createRouter, createWebHistory, RouteLocationNormalized} from 'vue-router'
import HomeView from '../views/HomeView.vue'
import {usePermissionStore} from "@/stores/permissions"
import {useUserStore} from "@/stores/user"
import Login from "../views/Login.vue"

const routes = [
    {
        path: '/',
        children: [
            {
                path: '',
                name: 'home',
                component: HomeView,
                meta: {
                    permissions: ['allow-home'],
                    auth: true,
                }
            },
            {
                path: '/about',
                name: 'about',
                component: () => import( '../views/AboutView.vue'),
                meta: {
                    permissions: ['allow-about'],
                    auth: true,
                },
            },
            {
                path: '/403',
                name: '403',
                component: () => import( '../views/NotFound.vue'),
                meta: {
                    auth: true,
                },
            },
        ]
    },
    {
        path: '/login',
        name: 'login',
        component: () => Login,
    }
]
const router = createRouter({
    history: createWebHistory(process.env.BASE_URL),
    routes
});


router.beforeEach((to, from, next) => {
    console.log(to.name)
    if (to.meta.auth && useUserStore().isLoggedIn) {
        if (isAllowed(to)) {
            return next();
        } else {
            return next({name: '403'});
        }
    } else if (to.meta.auth == undefined) {
        return next();
    } else {
        return next({name: 'login'})
    }

})

function isAllowed(to: RouteLocationNormalized) {
    const abilities = usePermissionStore().permissions;
    const permissions = to.meta.permissions as any;
    let is_allowed = false;
    if (permissions) {
        for (let i = 0; i < permissions.length; i++) {
            if (abilities.includes(permissions[i])) {
                is_allowed = true;
            }
        }
    } else {
        is_allowed = true;
    }
    return is_allowed;
}

export default router
